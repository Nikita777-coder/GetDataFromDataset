﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;

/// <summary>
/// Parsing of data
/// </summary>
internal sealed record GetData
{
    /// <summary>
    /// Types for weather
    /// </summary>
    public enum Type
    {
        Snow,
        Fog,
        Cold,
        Storm,
        Rain,
        Precipitation, 
        Hail
    }
    
    /// <summary>
    /// Types for Severity
    /// </summary>
    public enum Severity
    {
        Light,
        Severe,
        Moderate,
        Heavy, 
        UNK, 
        Other
    }
    
    // Field for head of table
    private string[] head; 
    // List for store data from dataset
    private List<LineFromData> lines = new List<LineFromData>();
    
    /// <summary>
    /// Method which parse string format of type weather to Type type
    /// </summary>
    /// <param name="data"> string format of type weather</param>
    /// <returns>Type type</returns>
    Type Parse(string data) => (Type) Enum.Parse(typeof(Type), data);
    
    /// <summary>
    /// Method which parse string format of severity to Severity type
    /// </summary>
    /// <param name="data">string format of severity</param>
    /// <returns>Severity type</returns>
    Severity Parse(string data, string date = "") => (Severity) Enum.Parse(typeof(Severity), data);
    
    /// <summary>
    /// Property which return parse data
    /// </summary>
    public List<LineFromData> Lines  => lines;
    
    /// <summary>
    /// Constructor for parsing data
    /// </summary>
    /// <param name="filePath">path to file</param>
    public GetData(string filePath)
    {
        // Read file with StreamReader.
        using (StreamReader fileInfo = new StreamReader(filePath))
        {
            // Read all rows of data
            while (!fileInfo.EndOfStream)
            {
                // Parse data
                string[] line = fileInfo.ReadLine().Split(',');
                
                // Get info about not head rows
                if (line[1] != "Type")
                {
                    // Convert data to right type 
                    Type memberType = Parse(line[1]);
                    Severity member = Parse(line[2], "");
                    DateTime startTime = DateTime.Parse(line[3]), endTime = DateTime.Parse(line[4]);

                    lines.Add(new() {EventId = line[0], Type = memberType, Severity = member, StartTime = 
                            startTime, EndTime = endTime, TimeZone = line[5], AirportCode = line[6], LocationLat = 
                            float.Parse(line[7], CultureInfo.InvariantCulture), 
                        LocationLng = float.Parse(line[8], CultureInfo.InvariantCulture), City = line[9], Country = 
                            line[10], State = line[11], ZipCode = line[12]});
                }
            }
        }
    }
}
