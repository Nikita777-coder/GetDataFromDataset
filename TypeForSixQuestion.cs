﻿/// <summary>
/// Type for six question
/// </summary>
internal sealed record LineFromDataForSixQuestion
{
    /// Properties for all fields of row of data from six question
    public double MeanTimeForTheMostCommonEvent { get; set; }
    public double MeanTimeForTheMostRareEvent { get; set; }
    public GetData.Type TheMostCommonEvent { get; set; }
    public GetData.Type TheMostRareEvent { get; set; }
    public int Year { get; set; }
}