﻿using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Interface for defining methods which search answers on questions
/// </summary>
interface IQuestion
{
    // method for finding answer on zero question
    int ZeroQuestion();
    // method for finding answer on first question
    (int, int) FirstQuestion();
    // method for finding answer on second question
    IEnumerable<IGrouping<string,LineFromData>> SecondQuestion();
    // method for finding answer on third question
    IEnumerable<LineFromData> ThirdQuestion();
    // method for finding answer on fourth question
    IEnumerable<IEnumerable<LineFromData>> FourthQuestion();
    // method for finding answer on fifth question
    IOrderedEnumerable<LineFromData> FifthQuestion();
    // method for finding answer on sixth question
    IEnumerable<LineFromDataForSixQuestion> SixthQuestion();
}