﻿using System;

/// <summary>
/// Type for data
/// </summary>
internal sealed record LineFromData
{ 
    /// <summary>
    /// Properties for all fields of row of data from dataset
    /// </summary>
    public string EventId { get; init; }
    public GetData.Type Type { get; init; }
    public GetData.Severity Severity { get; init; }
    public DateTime StartTime { get; init; }
    public DateTime EndTime { get; init; }
    public string TimeZone { get; init; }
    public string AirportCode { get; init; }
    public float LocationLat { get; init; }
    public float LocationLng { get; init; }
    public string City { get; init; }

    public string Country { get; init; }

    public string State { get; init; }

    public string ZipCode { get; init; }
}

