﻿using System;

/// <summary>
/// Additional methods of record AnswerOnQuestions
/// </summary>
interface IAdditionalFunctionMembers
{
    void Underlines(in string length);
    string Shifts(in int n);
    void HeadFormattingOfAnswer(string descriptionOfTask, string columns);
    void Action(string NumberOfQuestion, string numberOfWay, TimeSpan timeWay, TimeSpan timeLongestWay, TimeSpan totalTime);
    void Action(string numberOfQuestion, TimeSpan time);
}