﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Class for finding questions on answers and getting string presentation of answers
/// </summary>
internal sealed record AnswerOnQuestions : IAnswer, IQuestion, IAdditionalFunctionMembers, ITimeAnswers, IAnswersToConsole
{
    private Stopwatch stopWatch = new Stopwatch();
    private List<LineFromData> data = new List<LineFromData>();
    private Action act, acts;
    private List<Action> funcs = new List<Action>();
    // variable that was created for further simplicity of using timess in other methods
    private TimeSpan[] times = new TimeSpan[4];
    
    /// <summary>
    /// Get parse data from dataset
    /// </summary>
    /// <param name="data">parse data</param>
    public AnswerOnQuestions(List<LineFromData> data) => this.data = data;
    
    // Activation of methods that have field stopWatch
    private List<Action> GetAnswers
    {
        get
        {
            funcs.Add(ZeroAnswer);
            funcs.Add(FirstAnswer);
            funcs.Add(SecondAnswer);
            funcs.Add(ThirdAnswer);
            funcs.Add(FourthAnswer);
            funcs.Add(FifthAnswer);
            funcs.Add(SixthAnswer);
            return funcs;
        }
    }
    
    /// <summary>
    /// Invoke GetAnswers
    /// </summary>
    public void InvokeSearchingMethods()
    {
        var ans = GetAnswers;
        
        foreach (var el in ans)
        {
            el?.Invoke();
        }
    }
    
    /// <summary>
    /// Getting of time from each method which contains answer on question
    /// </summary>
    public void GetTimesOfFindingAnswers()
    {
        // Get string presentation of answer in console
        HeadFormattingOfAnswer("\n1. Time spent by searching methods\n", "Number of question\t|\t" +
                                                                         "Quantity of seconds\t|\tQuantity of milliseconds");
        act.Invoke();
        Console.WriteLine("\n");
    }
    
    /// <summary>
    /// Getting time answers from ways
    /// </summary>
    public void GetAnswersOnTime()
    {
        // Add four methods to Action: GetTimeOfFirstWay(), GetTimeOfSecondWay(), GetTimeOfThirdWay(), GetSumTimeOfWays()
        GetSumTimeOfWays();
        // Add remaining methods
        GetDifferentBetweenFirstFasterWayAndTheLongestWay();
        GetDifferentBetweenSecondFasterWayAndTheLongestWay();
        // Invoke adding methods to Action
        acts?.Invoke();
    }
    
    /// <summary>
    /// Getting sum time of ways
    /// </summary>
    /// <returns>sum time of ways</returns>
    public TimeSpan GetSumTimeOfWays()
    {
        // Get string presentation of each way in console
        HeadFormattingOfAnswer("2. Time spent by each way\n", "Number of way\t\t|\tQuantity of seconds" +
                                                              "\t|\tQuantity of milliseconds");
        var time1 = GetTimeOfFirstWay();
        times[0] = time1;
        var time2 = GetTimeOfSecondWay();   
        times[1] = time2;
        var time3 = GetTimeOfThirdWay();
        times[2] = time3;
        var sum = time1+time2+time3;
        times[3] = sum;
        Array.Sort(times);
        acts += () => Console.WriteLine($"\n\n3. Total spent time on calling methods in different ways is " +
                                        $"{sum.Seconds:00} seconds and {sum.Milliseconds:000} milliseconds");
        return sum;
    }
    
    /// <summary>
    /// Getting time from first way
    /// </summary>
    /// <returns>time from first way</returns>
    public TimeSpan GetTimeOfFirstWay()
    {
        var ans = GetAnswers;
        stopWatch.Restart();
        
        foreach (var el in ans)
        {
            el?.Invoke();
        }
        
        stopWatch.Stop();
        var time = stopWatch.Elapsed;

        acts += () => Action("1", time);

        return time;
    }
    
    /// <summary>
    /// Getting time from second way
    /// </summary>
    /// <returns>time from second way</returns>
    public TimeSpan GetTimeOfSecondWay()
    {
        var ans = GetAnswers;
        stopWatch.Restart();
        
        Parallel.ForEach(ans, act => act?.Invoke());
        
        stopWatch.Stop();
        var time = stopWatch.Elapsed;
        acts += () => Action("2", time);

        return time;
    }
    
    /// <summary>
    /// Getting time from third way
    /// </summary>
    /// <returns>time from third way</returns>
    public TimeSpan GetTimeOfThirdWay()
    {
        var ans = GetAnswers;
        var arr = new Task[ans.Count];
        
        stopWatch.Restart();
        
        for (int i = 0; i < arr.Length; ++i)
        {
            arr[i] = new Task(ans[i]);
        }

        foreach (var el in arr)
        {
            el.Start();
        }
        Task.WaitAll(arr);
        
        stopWatch.Stop();
        var time = stopWatch.Elapsed;
        acts += () => Action("3", time);
        
        return time;
    }
    
    /// <summary>
    /// Getting different between first faster way and the longest way and string presentation of answer in console
    /// </summary>
    public void GetDifferentBetweenFirstFasterWayAndTheLongestWay() => acts += () => Action(
        "4","2-nd", times.OrderBy(el => el).ToArray()[0],
        times.OrderByDescending(el => el).ToArray()[1], times[3]);
    
    /// <summary>
    /// Getting different between second faster way and the longest way and string presentation of answer in console
    /// </summary>
    public void GetDifferentBetweenSecondFasterWayAndTheLongestWay() => acts += () => Action(
        "5","3-rd", times.OrderBy(el => el).ToArray()[1],
        times.OrderByDescending(el => el).ToArray()[1], times[3]);
    
    public int ZeroQuestion() => data.Count(item => item.StartTime.Year == 2018 | item.EndTime.Year == 2018);
    
    public void ZeroAnswer()
    {
        stopWatch.Start();

        var ans = ZeroQuestion();

        stopWatch.Stop();
        var time = stopWatch.Elapsed;
        act += () => Action("0", time);
    }
    
    
    void IAnswersToConsole.ZeroAnswerToConsole()
    {
        var ans = ZeroQuestion();
        Console.WriteLine($"\n0. Quantity of natural phenomena is {ans}\n\n");
    }

    public (int, int) FirstQuestion() => (data.Select(item => item.City).Distinct().Count(),
        data.Select(item => item.State).Distinct().Count());

    public void FirstAnswer()
    {
        stopWatch.Start();

        var ans = FirstQuestion();
        var first = ans.Item1;
        var second = ans.Item2;
        stopWatch.Stop();
        var time = stopWatch.Elapsed;
        act += () => Action("1", time);
    }
    
    void IAnswersToConsole.FirstAnswerToConsole()
    {
        var ans = FirstQuestion();
        Console.WriteLine($"1.1. Quantity of states in dataset is {ans.Item2}\n1.2. Quantity of cities" +
                          $" in dataset is {ans.Item1}\n\n");
    }

    public IEnumerable<IGrouping<string, LineFromData>> SecondQuestion()
    {
        var listWithRain = data.Where(item => item.Type == GetData.Type.Rain);
        var rainIn2019 = listWithRain.Where(item => item.StartTime.Year == 2019);
        var groupByCity = rainIn2019.GroupBy(item => item.City);
        var answer = groupByCity.OrderByDescending(item => item.Count());
        var ans = answer.Take(3);

        return ans;
    }

    public void SecondAnswer()
    {
        stopWatch.Start();

        var ans = SecondQuestion();
        foreach (var el in ans)
        {
            
        }
        
        stopWatch.Stop();
        var time = stopWatch.Elapsed;
        act += () => Action("2", time);
    }

    void IAnswersToConsole.SecondAnswerToConsole()
    {
        var ans = SecondQuestion();
        HeadFormattingOfAnswer("\n2. Top-3 of the most rainy cities of 2019 year\n", "City\t\t\t|\tQuantity");
        foreach (var el in ans)
            Console.WriteLine($"{el.Key}{Shifts(24-el.Key.Length)}|\t{el.Count()}");
        Console.Write("\n\n");
    }

    public IEnumerable<LineFromData> ThirdQuestion()
    {
        var snowDays = data.Where(item => item.Type == GetData.Type.Snow);
        var group = snowDays.GroupBy(item => item.StartTime.Year);
        var answer = group.Select(item =>
        {
            var max = item.Max(i => i.EndTime - i.StartTime);
            return item.First(i => i.EndTime - i.StartTime == max);
        });

        return answer;
    }

    public void ThirdAnswer()
    {
        stopWatch.Start();

        var ans = ThirdQuestion();
        foreach (var el in ans)
        {
            
        }

        stopWatch.Stop();
        var time = stopWatch.Elapsed;
        act += () => Action("3", time);
    }

    void IAnswersToConsole.ThirdAnswerToConsole()
    {
        var ans = ThirdQuestion();
        HeadFormattingOfAnswer("\n\t\t3. Top 1 city in each year with the longest snowfall\n", 
            "Year\t|\tCity\t\t\t|\tStart Time\t\t|\tEnd Time");
        foreach (var el in ans)
        {
            Console.WriteLine($"{el.StartTime.Year}\t|\t{el.City}{Shifts(24 - el.City.Length)}|\t{el.StartTime}" +
                              $"\t|\t{el.EndTime}");
        } 
        Console.Write("\n\n");
    }

    public IEnumerable<IEnumerable<LineFromData>> FourthQuestion()
    {
        var year2019 = data.Where(item => item.StartTime.Year == 2019);
        var state = year2019.GroupBy(item => item.State);
        var events = state.Select(item => item.Where(
            i => i.EndTime - i.StartTime <= TimeSpan.Parse("02:00")));

        return events;
    }

    public void FourthAnswer()
    {
        stopWatch.Start();
        var ans = FourthQuestion();
        foreach (var el in ans)
        {
            foreach (var e in el)
            {
               break; 
            }
        }

        stopWatch.Stop();
        var time = stopWatch.Elapsed;
        act += () => Action("4", time);
    }

    void IAnswersToConsole.FourthAnswerToConsole()
    {
        var ans = FourthQuestion();
        
        HeadFormattingOfAnswer("\n4. Quantity of events that were not over 2 hours in each State\n", "" +
            "City\t\t\t|\tQuantity");
        foreach (var el in ans)
        {
            foreach (var e in el)
            {
                Console.WriteLine($"{e.City}{Shifts(24 - e.City.Length)}|\t{el.Count()}");
                break;
            }
        }
        Console.WriteLine("\n");
    }

    public IOrderedEnumerable<LineFromData> FifthQuestion()
    {
        var year2017 = data.Where(item => item.StartTime.Year == 2017 && item.Severity
            == GetData.Severity.Severe);
        var stateCity = year2017.GroupBy(item => item.State);
        var max = stateCity.Select(i =>
        {
            var maxum = i.Max(it => (it.EndTime - it.StartTime).TotalHours);
            return i.First(it => (it.EndTime - it.StartTime).Hours - maxum < 1e-2);
        });
        var ans = max.OrderByDescending(i => (i.EndTime - i.StartTime).TotalHours);

        return ans;
    }

    public void FifthAnswer()
    {
        stopWatch.Start();

        var ans = FifthQuestion();
        foreach (var el in ans)
        {
            
        }

        stopWatch.Stop();
        var time = stopWatch.Elapsed;
        act += () => Action("5", time);
    }

    void IAnswersToConsole.FifthAnswerToConsole()
    {
        var ans = FifthQuestion();
        
        HeadFormattingOfAnswer("\n     5. The longest sum durations of events in the specific city of each state\n", 
            "State\t|\t\tCity\t\t|\tSum durations of events in hours");
        foreach (var el in ans)
        {
            Console.WriteLine($"{el.State}\t|\t{el.City}{Shifts(Math.Abs(24 - el.City.Length))}|\t" +
                              $"{Math.Round((el.EndTime - el.StartTime).TotalHours, 2)}");
        }
        
        Console.Write("\n\n");
    }

    public IEnumerable<LineFromDataForSixQuestion> SixthQuestion()
    {
        var years = data.GroupBy(i => i.StartTime.Year);
        var events = years.Select(i =>
        {
            var groupByEvent = i.GroupBy(el => el.Type);
            var theMostCommonEvent = groupByEvent.Max(el => el.Count());
            var theMostRareEvent = groupByEvent.Min(el => el.Count());
            var variable = groupByEvent.Where(el => el.Count() == 
                theMostCommonEvent || el.Count() == theMostRareEvent);
            var ans = new LineFromDataForSixQuestion();
            ans.Year = i.Key;
            foreach (var el in variable)
            {
                // Min common event has count > 90000
                if (el.Count() > 90000)
                {
                    ans.TheMostCommonEvent = el.Key;
                    ans.MeanTimeForTheMostCommonEvent = el.Average(i => (i.EndTime - i.StartTime).TotalHours);
                }

                else
                {
                    ans.TheMostRareEvent = el.Key;
                    ans.MeanTimeForTheMostRareEvent = el.Average(i => (i.EndTime - i.StartTime).TotalHours);
                }
            }

            return ans;
        });

        return events;
    }

    public void SixthAnswer()
    {
        stopWatch.Start();

        var ans = SixthQuestion();
        foreach (var el in ans)
        {
            
        }

        stopWatch.Stop();
        var time = stopWatch.Elapsed;
        act += () => Action("6", time);
    }

    void IAnswersToConsole.SixthAnswerToConsole()
    {
        var ans = SixthQuestion();
        
        HeadFormattingOfAnswer("\n6. Mean duration of the most rare and common events in each year\n", 
            "Year\t|\tThe most common event\t|\tThe most rare event\t|\tMean duration of common event " +
            "(hours)\t|\tMean duration of rare event (hours)");
        foreach (var el in ans)
        {
            Console.WriteLine($"{el.Year}\t|\t\t{el.TheMostCommonEvent}\t\t|\t\t{el.TheMostRareEvent}\t\t|\t\t\t" +
                              $"{Math.Round(el.MeanTimeForTheMostCommonEvent, 2)}" +
                              $"\t\t\t|\t\t\t{Math.Round(el.MeanTimeForTheMostRareEvent, 2)}");
        }
        
        Console.Write("\n\n");
    }
    /// <summary>
    /// Getting underlines for string
    /// </summary>
    /// <param name="str">variable that contains names and formatting of columns of table</param>
    public void Underlines(in string str)
    {
        foreach (var el in str)
        {
            // feature to \t
            if (el == '\t')
                Console.Write("------");
            Console.Write("-");
        }

        Console.WriteLine();
    }
    
    /// <summary>
    /// Method which insert missing shifts for table
    /// </summary>
    /// <param name="n">quantity of missing shifts</param>
    /// <returns>missing shifts</returns>
    public string Shifts(in int n)
    {
        StringBuilder s = new StringBuilder();
        for (int i = 0; i < n; i++)
        {
            s.Append(" ");
        }

        return s.ToString();
    }
    
    /// <summary>
    /// Pattern for head of table
    /// </summary>
    /// <param name="descriptionOfTask">task</param>
    /// <param name="columnsInStringFormat">columns of table with string formatting</param>
    public void HeadFormattingOfAnswer(string descriptionOfTask, string columnsInStringFormat)
    {
        Console.WriteLine(descriptionOfTask);
        Console.WriteLine(columnsInStringFormat);
        Underlines(columnsInStringFormat);
    }
    
    /// <summary>
    /// Formatting of date to table
    /// </summary>
    /// <param name="numberOfQuestion"></param>
    /// <param name="time"></param>
    public void Action(string numberOfQuestion, TimeSpan time) => Console.WriteLine($"\t{numberOfQuestion}\t\t|\t\t" +
        $"{time.Seconds:00}\t\t|\t\t{time.Milliseconds:000}");
    
    /// <summary>
    /// Formatting of methods that have "Different" in name
    /// </summary>
    /// <param name="numberOfQuestion"></param>
    /// <param name="numberOfWay"></param>
    /// <param name="timeWay"></param>
    /// <param name="timeLongestWay"></param>
    /// <param name="totalTime"></param>
    public void Action(string numberOfQuestion, string numberOfWay, TimeSpan timeWay, TimeSpan timeLongestWay, TimeSpan 
        totalTime) => Console.WriteLine($"{numberOfQuestion}. The {numberOfWay} way was faster than the 1-st way on " +
                                        $"{Math.Round((timeLongestWay - timeWay) / totalTime * 100, 2)}%");
}