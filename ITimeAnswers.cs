﻿using System;

/// <summary>
/// Interface for defining methods for time answers
/// </summary>
interface ITimeAnswers
{
    // Method for getting time of each method that search answer on question
    void GetTimesOfFindingAnswers();
    
    // Method for getting answers to the questions that were on the search for the time of each path
    void GetAnswersOnTime();
    // Method for getting sum of time of all ways 
    TimeSpan GetSumTimeOfWays();
    // Method for getting time of first way
    TimeSpan GetTimeOfFirstWay();
    // Method for getting time of second way
    TimeSpan GetTimeOfSecondWay();
    // Method for getting time of third way
    TimeSpan GetTimeOfThirdWay();
    // Method for getting Different Between First Faster Way And The Longest Way
    void GetDifferentBetweenFirstFasterWayAndTheLongestWay();
    // Method for getting Different Between Second Faster Way And The Longest Way
    void GetDifferentBetweenSecondFasterWayAndTheLongestWay();
}