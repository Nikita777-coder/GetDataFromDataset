﻿/// <summary>
/// Interface for defining methods which output to console presentation of question 
/// </summary>
interface IAnswersToConsole
{
    // Method for getting string presentation of zero question to console
    void ZeroAnswerToConsole();
    // Method for getting string presentation of first question to console
    void FirstAnswerToConsole();
    // Method for getting string presentation of second question to console
    void SecondAnswerToConsole();
    // Method for getting string presentation of third question to console
    void ThirdAnswerToConsole();
    // Method for getting string presentation of fourth question to console
    void FourthAnswerToConsole();
    // Method for getting string presentation of fifth question to console
    void FifthAnswerToConsole();
    // Method for getting string presentation of sixth question to console
    void SixthAnswerToConsole();
}
        