﻿/// <summary>
/// Interface for defining methods which define time of finding answer
/// </summary>
interface IAnswer
{
    // method for getting time on finding answer on zero question
    void ZeroAnswer();
    // method for getting time on finding answer on first question
    void FirstAnswer();
    // method for getting time on finding answer on second question
    void SecondAnswer();
    // method for getting time on finding answer on third question
    void ThirdAnswer();
    // method for getting time on finding answer on fourth question
    void FourthAnswer();
    // method for getting time on finding answer on fifth question
    void FifthAnswer();
    // method for getting time on finding answer on sixth question
    void SixthAnswer();
}