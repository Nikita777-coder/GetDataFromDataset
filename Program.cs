﻿using System;
using System.IO;

class Programm
{
    public static void Main()
    {
        GetData data = new(Directory.GetCurrentDirectory() + @"\WeatherEvents_Jan2016-Dec2020.csv");
        AnswerOnQuestions answers = new AnswerOnQuestions(data.Lines); 
        answers.InvokeSearchingMethods();
        answers.GetTimesOfFindingAnswers();
        answers.GetAnswersOnTime(); 
        
        Console.WriteLine();
        answers.Underlines("Outputting of string presentation of answers on questions");
        Console.WriteLine("Outputting of string presentation of answers on questions");
        answers.Underlines("Outputting of string presentation of answers on questions");
        (answers as IAnswersToConsole).ZeroAnswerToConsole();
        (answers as IAnswersToConsole).FirstAnswerToConsole();
        (answers as IAnswersToConsole).SecondAnswerToConsole();
        (answers as IAnswersToConsole).ThirdAnswerToConsole();
        (answers as IAnswersToConsole).FourthAnswerToConsole();
        (answers as IAnswersToConsole).FifthAnswerToConsole();
        (answers as IAnswersToConsole).SixthAnswerToConsole();
    }
}